
* I do not own this project!

This project is not maintained by me, I only package it for Arch and Debian Linux and derivative GNU/Linux distributions. Therefore I cannot provide support for it either, so please, only open issues if the problem is probably related to the package itself (p.e. unclear installation instructions, missing files, etc.), but not the source code of the launcher, thank you 💓.

Original developers: https://tlaun.ch/

* How to install

** Prerequisites
- pacman or dpkg
- either `git` or any archiving program
- root privileges

** Download

*** with git:
- ~git clone https://gitlab.com/TypoMustakes/tl-legacy.git~
*** without git:
- visit [[https://gitlab.com/TypoMustakes/tl-legacy][my repository]]
- click the  (Download) button and choose and archive format
- extract the archive

** Installation

 Please note that out of the following three options,
 only one is necessary to successfully install the package.

 Open the project directory by typing `cd tl-legacy`

*** installing existing package
**** Arch

~sudo pacman -U tl-legacy-*-any.pkg.tar.zst~

**** Debian

~sudo dpkg -i tl-legacy_*.deb~

*** build your own package:

Note: as I am not yet familiar with the Debian packaging system, I only built the .deb package in a very amateur way and therefore cannot yet provide you with a concrete way to do it yourself.

Therefore, this command only applies to Arch or derivative GNU/Linux systems, running pacman.

You can still package it for Debian yourself though, just don't blame me if it goes wrong.

**** Arch
 
 ~makepkg -si~

*** add repository (Arch only)

 You can also [[https://gitlab.com/TypoMustakes/typomustakes][add my repository]], after which you will be able to install it like any other program, using your package manager!


* Have a nice day!
